# git-learning

This is a git learning Repository.

### 集中式工作流

git push origin master (fail)

git pull --rebase origin master

git add <some-file>

git rebase --continue / git rebase --abort

git push origin master


### 功能分支工作流

git checkout -b marys-feature master

git status

git add <some-file>

git commit

git push -u origin marys-feature
(-u 设置跟踪分支: git push eq git push origin marys-feature)

git checkout master

git pull

git pull origin marys-feature

git push


### Gitflow工作流
master	存储正式发布的历史
develop	作为功能的集成分支

每个新功能位于一个自己的分支，这样可以push到中央仓库以备份和协作。
但功能分支不是从master分支上拉出新分支，而是使用develop分支作为父分支。
当新功能完成时，合并回develop分支。新功能提交应该不直接与master分支交互。

发布分支，从develop分支上checkout，不追加新功能，只应该做Bug修复，文档生成和其他面向发布任务。
工作完成后，发布分支合并到master分支并分配版本号打好Tag。并且合并回develop分支。
release-* / release/*

hotfix分支，用于给production releases快速生成补丁，这是唯一可以直接从master分支fork出来的分支。
修复完成后，合并回master和develop

git branch develop

git push -u origin develop

git clone ssh://user@host/path/to/repo.git

git checkout -b dvelop origin/develop

git checkout -b some-feature develop

git status

git add <some-file>

git commit

git pull origin develop

git checkout develop

git merge some-feature

git push

git branch -d some-feature

git checkout -b release-0.1 develop
(只要创建这个分支并push到中央仓库，这个发布就是功能冻结的。任何不在develop分支中的新功能都推到下个发布循环中)

git checkout master

git merge release-0.1

git push

git checkout develop

git merge release-0.1

git push

git branch -d release-0.1

git tag -a 0.1 -m "Initial public release" master

git push --tags

git checkout -b issue-#001 master

\# fix the bug

git checkout master

git merge issue-#001

git push

git checkout develop

git merge issue-#001

git push

git branch -d issue-#001


### Forking 工作流

开发者developer	不信任贡献者contributor

ssh user@host

git init --bare /path/to/repo.git

Fork

git clone https://user@host/user/repo.git

git remote add upstream https://bitbucket.org/maintainer/repo

git remote add upstream https://user@bitbucket.org/maintainer/repo.git

git checkout -b some-feature

coding

git commit -a -m "Add first draft of some feature"

git pull upstream master

git push origin feature-branch

pull request

git fetch https://bitbucket.org/user/repo feature-branch

git checkout master

git merge FETCH-HEAD

git push origin master

git pull upstream master
