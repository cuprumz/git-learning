git remote

git remote -v

git remote add <remote-name> <remote-url>

git fetch <remote-name>

git remote rename <current-name> <new-name>

git remote rm <remote-name>
