### git reset [<mode>] [<commit>]

<mode>: --mixed(default/omitted) / --soft / --hard / --merge / --keep
--mixed:
Resets the index but not the working tree and reports what has not been updated. This is the default action.
--soft:
Does not touch the index file or the working tree at all. This leaves all your changed file "Changes to be committed", as "git status" would put it.
--hard:
Resets the index and working tree. Any changes to tracked files in the working tree since <commit> are discarded.
--merge:
TODO
--keep:
TODO
