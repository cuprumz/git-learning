### git diff

Changes in the working tree not yet staged for the next commit.


### git diff --cached

Changes between the index and your last commit; what you would be committing if you run "git commit"
without "-a" option.


### git diff HEAD

Changes in the working tree since your last commit; what you would be committing if you run "git commit -a"
